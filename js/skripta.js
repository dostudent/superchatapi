$(document).ready(function() {
    var baseUrl = "https://chat.data-lab.si/api";
    var user = {id: 69, name: "David Ocepek"};
    var nextMessageId = 0;
    var currentRoom = "Skedenj";
    var sobe;

    //Naloži seznam sob
    var pridobiSobe = function(){
        $.ajax({
            url: baseUrl + "/rooms",
            type: "GET",
            success: function (data) {
                $("#rooms").html("");
                for (i in data) {
                    $("#rooms").append(" \
                        <li class='media' id='" + data[i] + "'> \
                            <div class='media-body room' style='cursor: pointer;'> \
                                <div class='media'> \
                                    <a class='pull-left' href='#'> \
                                        <img class='media-object img-circle' src='img/" + data[i] + ".jpg' /> \
                                    </a> \
                                    <div class='media-body'> \
                                        <h5>" + data[i] + "</h5> \
                                    </div> \
                                </div> \
                            </div> \
                        </li>");
                }
                var sobe = document.querySelector("#rooms");
                sobe.addEventListener('click', function(event){
                    menjavaSobe(event);
                });
            }
        });
    };
    pridobiSobe();

    //Definicija funkcije za pridobivanje pogovorov, ki se samodejno ponavlja na 5 sekund
    var pridobiSporocila = function(){
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom + "/" + nextMessageId,
            type: "GET",
            success: function(data){
                nextMessageId = data[data.length - 1].id + 1;
                for(i in data){
                    $("#messages").append(" \
                    <li class='media'> \
                        <div class='media-body'> \
                            <div class='media'> \
                                <a class='pull-left' href='#'> \
                                    <img class='media-object img-circle' src='https://d2ln1xbi067hum.cloudfront.net/assets/default_user-abdf6434cda029ecd32423baac4ec238.png' /> \
                                </a> \
                                <div class='media-body' > \
                                <small class='text-muted'>" + data[i].user.name + "|" + data[i].time + "</small> <br />" +
                                data[i].text + 
                                    "<hr /> \
                                </div> \
                            </div> \
                        </div> \
                    </li>");
                }
            }
        });
        
    }
    pridobiSporocila();
    
    //Definicija funkcije za posodabljanje seznama uporabnikov, ki se samodejno ponavlja na 5 sekund
    var pridobiUporabnike = function(){
        $.ajax({
           url: baseUrl + '/users/' + currentRoom,
           method: 'GET',
           success: function(data){
               $("#users").html("");
               for(i in data){
                   $('#users').append(' \
                   <li class="media"> \
                        <div class="media-body"> \
                            <div class="media"> \
                                <a class="pull-left" href="#"> \
                                    <img class="media-object img-circle" src="https://d2ln1xbi067hum.cloudfront.net/assets/default_user-abdf6434cda029ecd32423baac4ec238.png" />\
                                </a> \
                                <div class="media-body" > \
                                    <h5>' + data[i].name + '</h5> \
                                </div> \
                            </div> \
                        </div> \
                    </li>');
                   
               }
           }
        });
    }
    pridobiUporabnike();

    //Definicija funkcije za pošiljanje sporočila
    //Definicija funkcije za pošiljanje sporočila
    var posljiSporocilo = function(){
        var sporocilo = {
            "user": user,
            "id": nextMessageId,
            "time": "2017-08-27 14:12:19",
            "text": $('#message').val()
        };
        $('#message').val("");
        
        $.ajaxSetup({
            headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
        });
        $.ajax({
            url: baseUrl + '/messages/' + currentRoom,
            method: 'POST',
            contentType: 'aplication/json',
            data: JSON.stringify(sporocilo),
            error: function(){
                alert('Prišlo je do napake pri pošiljanju sporočila. Prosimo poskusite znova!');
            }
        });
    };
    
    $("#send").click(posljiSporocilo);

    //Definicija funkcije za menjavo sobe (izbriši pogovore in uporabnike na strani, nastavi spremenljivko currentRoom, nastavi spremenljivko nextMessageId na 0)
    var menjavaSobe = function(event){
        var soba = event.target.closest("li").id;
        if(soba != currentRoom){
            $("#users").html("");
            $("#messages").html("");
            currentRoom = soba;
            nextMessageId = 0;
            pridobiSobe();
            pridobiSporocila();
            pridobiUporabnike();
        }
    }
    
    setInterval(pridobiSporocila, 5000);
    setInterval(pridobiUporabnike, 5000);
});